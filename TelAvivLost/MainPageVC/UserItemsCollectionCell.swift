//
//  UserItemsCollectionCell.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 26/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class UserItemsCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    
    @IBOutlet weak var actionLabel: UILabel!
    
    @IBOutlet weak var itemName: UILabel!
    
    @IBOutlet weak var localTime: UILabel!
}
