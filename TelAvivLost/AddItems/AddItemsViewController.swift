//
//  AddItemsViewController.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 22/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class AddItemsViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var itemCategoryTextField: UITextField!
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var itemSubtitleTextField: UITextField!
    @IBOutlet weak var itemDescriptionTextField: UITextView!
    
    @IBOutlet weak var pickerView: UIPickerView!
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var saveItemToDB: UIButton!
    
    var lostItemID = ""
    var foundItemID = ""
    
    let uid = Auth.auth().currentUser?.uid
    
    let categories = ["Mobile Phone", "Wallets", "IDs", "Documents", "Clothes", "Animals", "Other"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.isHidden = true
        
        segmentedControl.selectedSegmentIndex = 0

    }
    
    @IBAction func startedCategories(_ sender: UITextField) {
        
        pickerView.isHidden = false
        saveItemToDB.isHidden = true
    }
    

    
    @IBAction func endCategories(_ sender: Any) {
        pickerView.isHidden = true
        saveItemToDB.isHidden = false
    }
    
    
    @IBAction func saveItemButton(_ sender: UIButton) {
        
        SVProgressHUD.show()
        
        if segmentedControl.selectedSegmentIndex == 0 {
            
            guard let name = itemNameTextField.text, let subtitle = itemSubtitleTextField.text, let description = itemDescriptionTextField.text, let category = itemCategoryTextField.text, let index = Database.database().reference().childByAutoId().key else {return}
            
            self.lostItemID = index
            
            let values = ["name": name, "subtitle": subtitle, "description": description, "category": category, "uid": uid, "index": index, "profileImageUrl": prof]
            
            let DB = Database.database().reference()
            DB.child("lost").child(index).updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(snapshot, error) in
                
                if error != nil {
                    
                    print("Saved")


                    let ref = Database.database().reference().child("userItems").child(self.uid!).child(index)
                    ref.updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(error, ref) in
                        
                        
                        if error != nil {
                            
                            print("Saved")
                            
                            SVProgressHUD.dismiss()
                            self.addMedicationAlert()
                            self.performSegue(withIdentifier: "ItemsVC", sender: self)
                        }
                        return
                        
                    })

                    
                    self.performSegue(withIdentifier: "ItemsVC", sender: self)
                    
                } else {
                    print("not saved")
                }
                
            })
            
        } else {
            
            guard let name = itemNameTextField.text, let subtitle = itemSubtitleTextField.text, let description = itemDescriptionTextField.text, let category = itemCategoryTextField.text, let index = Database.database().reference().childByAutoId().key else {return}
            
            let values = ["name": name, "subtitle": subtitle, "description": description, "category": category, "uid": uid, "index": index]
            
            self.foundItemID = index
            
            let DB = Database.database().reference()
            DB.child("found").child(index).updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(snapshot, error) in
                
                if error != nil {
                    
                    print("Saved")

                    SVProgressHUD.dismiss()
                    self.addMedicationAlert()
                    
                    let ref = Database.database().reference().child("userItems").child(self.uid!).child(index)
                    ref.updateChildValues(values as [AnyHashable : Any])
                    
                    self.performSegue(withIdentifier: "ItemsInfoVC", sender: self)
                } else {
                    print("not saved")
                }
                
            })
            
        }

        

        
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        itemCategoryTextField.text = "\(categories[row])"
    }
    
    
    

}
