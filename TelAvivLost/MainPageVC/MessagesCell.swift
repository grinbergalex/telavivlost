//
//  MessagesCell.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 24/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class MessagesCell: UITableViewCell {
    
    @IBOutlet weak var messageTextField: UILabel!
    @IBOutlet weak var date: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
