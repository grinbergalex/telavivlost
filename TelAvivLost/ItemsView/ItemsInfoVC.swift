//
//  FirstViewController.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 22/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import FirebaseAuth
import SVProgressHUD

class ItemsInfoVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var delegate: CenterVCDelegate?
    
    var itemsToLoad = [Items]()
    
    var userMessages = [Messages]()
    
    var users = [User]()
    
    var posts = [Post]()
    
    var backInfo = [backInformation]()
    
    var addressAray = [AddressFromUser]()
    var address = AddressFromUser()
    
    var addressFinalForUser = InfoVC?.self
    
    var key = Database.database().reference().childByAutoId().key
    
    let uid = Auth.auth().currentUser?.uid
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
       // fetchUser()
        fetchAddsForUsers()
        SVProgressHUD.dismiss()
        
    
        
    }
    //Updated this functions
    func fetchAddsForUsers() {

        let DB_BASE = Database.database().reference().child("lost")
            DB_BASE.observe(.childAdded, with: { (snapshot) in
                
                if let dictionary = snapshot.value as? [String: AnyObject] {
                    let item = Items()
                    item.itemName = (dictionary["name"] as! String?)!
                    item.itemImage = (dictionary["subtitle"] as! String?)!
                    item.itemSubtitle = (dictionary["description"] as! String?)!
                    item.key = (dictionary["index"] as! String?)!
                    item.itemImage = (dictionary["profileImageUrl"] as! String?)!
                    self.itemsToLoad.append(item)
                    
                    let url = URLRequest(url: URL(string: item.itemImage)!)
                    
                    let task = URLSession.shared.dataTask(with: url) {
                        (data, response, error) in
                        
                        
                        
                        if error != nil {
                            print(error!)
                            return
                        }
                        
                    }
                    
                    task.resume()
                    
                    
                    
                    DispatchQueue.main.async(execute: {
                        self.collectionView?.reloadData()
                    })
                }
                print(snapshot)
            }, withCancel: nil)

    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsToLoad.count
    }
    
    func getImage(url: String, completion: @escaping (UIImage?) -> ()) {
        URLSession.shared.dataTask(with: URL(string: url)!) { data, response, error in
            if error == nil {
                completion(UIImage(data: data!))
            } else {
                completion(nil)
            }
            }.resume()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MainViewCell", for: indexPath) as! MainViewCell
        
        let index = itemsToLoad[indexPath.row]
        cell.loadItemName.text = index.itemName
        
        cell.loadItemImage.image = nil
        
        cell.tag += 1
        let tag = cell.tag
        
        let photoUrl =  itemsToLoad[indexPath.row].itemImage  //posts[indexPath.row].photoUrl
        
        getImage(url: photoUrl) { photo in
            if photo != nil {
                if cell.tag == tag {
                    DispatchQueue.main.async {
                        cell.loadItemImage.image = photo
                    }
                }
            }
        }
        
        return cell

    }

    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let items = itemsToLoad[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfoVC") as! InfoVC
        vc.itemsToLoad = items
        
        
        let ref = Database.database().reference().child("selections").child(key!)
        let index = itemsToLoad[indexPath.row]

        let values = ["itemNameText": index.itemName,"itemDescription": index.itemSubtitle, "itemId": key, "itemSubtitle": index.itemSubtitle, "profileImageUrl": index.itemImage]
        ref.updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(user, error) in
            if error != error {
                print("saved")
            }
            print("error")
        })
        
        
        
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func menuSideOpened(_ sender: UIButton) {
        
        self.delegate?.toggleLeftPanelVC()
        
    }
    
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        
        if segmentedControl.selectedSegmentIndex == 1 {
            self.performSegue(withIdentifier: "Found", sender: self)
        }
        
    }
    

}
