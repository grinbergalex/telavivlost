//
//  Alerts.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 25/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

extension UIViewController {
    
    func userLoggedInNotification() {
        
        var timer = Timer()
        
        let content = UNMutableNotificationContent()
        content.title = "User Logged in"
        content.body = "Login Successfully"
        content.badge = 1
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false);
        
        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func userRegisterNotification() {
        
        var timer = Timer()
        
        let content = UNMutableNotificationContent()
        content.title = "User Registered successfully"
        content.body = "Start enjoying the application"
        content.badge = 1
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false);
        
        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func addMedicationAlert() {
        
        var timer = Timer()
        
        let content = UNMutableNotificationContent()
        content.title = "User Registered successfully"
        content.body = "Start enjoying the application"
        content.badge = 1
        content.sound = UNNotificationSound.default
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false);
        
        let request = UNNotificationRequest(identifier: "timerDone", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
