//
//  SideMenuVC.swift
//  ReminderHealth
//
//  Created by Alex Grinberg on 18/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import QuartzCore

enum SlideOutState {
    case collapsed
    case leftPanelExpanded
}

enum ShowWhichVC {
    case homeVC
}

var showVC: ShowWhichVC = .homeVC

class ContainerVC: UIViewController {
    
    var homeVC: ItemsInfoVC!
    var leftVC: LeftSidePanelVC!
    var currentState: SlideOutState = .collapsed
    var centerController: UIViewController!
    
    var isHidden = false
    let centerPanelExpendedOffSet: CGFloat = 210
    
    
    var tap: UITapGestureRecognizer!
    
    var tap2: UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initCenter(screen: showVC)
      //  initCenterRight(screen: showVC)
        

    }
    
    func initCenterRight(screen: ShowWhichVC) {
        var presentingController: UIViewController
        
        showVC = screen
        
        if homeVC == nil {
            homeVC = UIStoryboard.homeVC()
            homeVC.delegate = self
        }
        
        presentingController = homeVC
        if let con = centerController {
            con.view.removeFromSuperview()
            con.removeFromParent()
        }
        
        centerController = presentingController
        
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    func initCenter(screen: ShowWhichVC) {
        var presentingController: UIViewController
        
        showVC = screen
        
        if homeVC == nil {
            homeVC = UIStoryboard.homeVC()
            homeVC.delegate = self
        }
        
        presentingController = homeVC
        if let con = centerController {
            con.view.removeFromSuperview()
            con.removeFromParent()
        }
        
        centerController = presentingController
        
        view.addSubview(centerController.view)
        addChild(centerController)
        centerController.didMove(toParent: self)
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    override var prefersStatusBarHidden: Bool {
        return isHidden
    }
    

}

extension ContainerVC: CenterVCDelegate {
        
    func toggleLeftPanelVC() {
        let notAlreadyExpanded = currentState != .leftPanelExpanded
        
        if notAlreadyExpanded {
            addLeftPanelViewController()
        }
        animateLeftPanel(shouldExpand: notAlreadyExpanded)
    }
    
    func addLeftPanelViewController() {
        
        if leftVC == nil {
            leftVC = UIStoryboard.leftViewVC()
            addChildSidePanelViewController(leftVC!)
            
        }
        
    }
    
    func addChildSidePanelViewController(_ sidePanelController: LeftSidePanelVC) {
        view.insertSubview(sidePanelController.view, at: 0)
        addChild(sidePanelController)
        sidePanelController.didMove(toParent: self)
    }
    
    
    
    //ANimate the left side panel:
    @objc func animateLeftPanel(shouldExpand: Bool) {
        
        if shouldExpand {
            
            isHidden = !isHidden
            animateStatusBar()
            setupWhiteCoverView()
            currentState = .leftPanelExpanded
            
            animateCenterPanelXPosistion(targetPosition: centerController.view.frame.width - centerPanelExpendedOffSet)
        } else {
            
            isHidden = !isHidden
            animateStatusBar()
            hideWhiteCoverView()
            
            animateCenterPanelXPosistion(targetPosition: 0) { (finished) in
                if finished == true {
                    self.currentState = .collapsed
                    self.leftVC = nil
                }
            }
        }
        
    }

    
    func setupWhiteCoverView() {
        
        let whiteCoverView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        whiteCoverView.backgroundColor = .white
        whiteCoverView.tag = 25
        whiteCoverView.alpha = 0.0
        
        self.centerController.view.addSubview(whiteCoverView)
        UIView.animate(withDuration: 0.2) {
            whiteCoverView.alpha = 0.75
        }
        
        tap = UITapGestureRecognizer(target: self, action: #selector(animateLeftPanel(shouldExpand:)))
        self.centerController.view.addGestureRecognizer(tap)
        
    }
    
    
    func hideWhiteCoverView() {
        centerController.view.removeGestureRecognizer(tap)
        for subview in self.centerController.view.subviews {
            if subview.tag == 25 {
                UIView.animate(withDuration: 0.2, animations: {
                    subview.alpha = 0.0
                    
                }, completion: {(finished) in
                    subview.removeFromSuperview()
                    
                })
            }
        
            }
        }
    
    func hideWhiteRightCoverView() {
        centerController.view.removeGestureRecognizer(tap)
        for subview in self.centerController.view.subviews {
            if subview.tag == 28 {
                UIView.animate(withDuration: 0.2, animations: {
                    subview.alpha = 0.0
                    
                }, completion: {(finished) in
                    subview.removeFromSuperview()
                    
                })
            }
            
        }
    }
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0 , options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        })
    }
    
    
    func animateCenterPanelXPosistion(targetPosition: CGFloat, completion: ((Bool) -> Void)! = nil) {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0 , options: .curveEaseInOut, animations: {
            self.centerController.view.frame.origin.x = targetPosition
            }, completion: completion)
    }
    
}

private extension UIStoryboard {
    
    class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func homeVC() -> ItemsInfoVC? {
        return mainStoryboard().instantiateViewController(withIdentifier: "ItemsInfoVC") as? ItemsInfoVC
    }
    
    class func leftViewVC() -> LeftSidePanelVC {
        return mainStoryboard().instantiateViewController(withIdentifier: "LeftSidePanelVC") as! LeftSidePanelVC
    }
    
}
