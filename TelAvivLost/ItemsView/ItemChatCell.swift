//
//  ItemChatCell.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 01/02/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class ItemChatCell: UITableViewCell {
    
    @IBOutlet weak var messagesText: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
