//
//  Categories.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 22/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Categories: NSObject {
    var categoryLabel = ""
    
    var profileImage: UIImageView!
}

class User: NSObject {
    var id: String?
    var name: String?
    var email: String?
    var profileImageUrl: String?
    init(dictionary: [String: AnyObject]) {
        self.id = dictionary["id"] as? String
        self.name = dictionary["name"] as? String
        self.email = dictionary["email"] as? String
        self.profileImageUrl = dictionary["profileImageUrl"] as? String
    }
}

class Items: NSObject {
    var itemName = ""
    var itemImage = ""
    var itemSubtitle = ""
    var itemImageView: UIImageView!
    var key = Database.database().reference().childByAutoId().key
    

    }

class UserProfilePictureFromSideMenu: NSObject {
    var profilePicUrl = ""
    var profileImageView: UIImageView!
    
    
}


class Messages: NSObject {
    var userName = ""
    var userDestionation = ""
    var fromUser = ""
    var itemImage = UIImageView()
    var date = ""
    var ToUser = ""
    var messageText = ""
}

class UserPersonalInformation: NSObject {
    var userImage = UIImageView()
    var username = ""
    var productDescription = ""
}

class MapInfo: NSObject {
    
    var latitude = ""
    var longitude = ""
}

class LoadMainInfo: NSObject {
    
    var username = ""
    var useremail = ""
    var password = ""
    var uid = ""
    var itemImage = UIImageView()
    var url = ""
    
}

class LoadAddedItems: NSObject {
    
    var itemName = ""
    var itemSubtitle = ""
    var itemDescription = ""
    var uid = ""
    var itemImage = UIImageView()
    
}

class AddressFromUser: NSObject {
    var addressLabel = ""
}

class backInformation: NSObject {
    
    var itemInfomation = ""
    var itemImage = ""
    var itemName = ""
    var profileImageUrl = ""
    
}

    
    class Post {
        var caption: String
        var photoUrl: String
        
        init(captiontxt: String, photoUrlString: String) {
            caption = captiontxt
            photoUrl = photoUrlString
            
            }
        }




