//
//  CenterVCDelegate.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 26/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import Foundation
import UIKit

protocol CenterVCDelegate {
    func toggleLeftPanelVC()
    func addLeftPanelViewController()
    func animateLeftPanel(shouldExpand: Bool)

}

protocol TopVCDelegate {
    func toggleTopPanelVC()
    func addTopPanelViewController()
    func animateTopPanel(shouldExpand: Bool)
    
}
