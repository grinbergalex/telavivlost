//
//  MainViewCell.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 22/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class MainViewCell: UICollectionViewCell {
    
    @IBOutlet weak var loadItemName: UILabel!
    @IBOutlet weak var loadItemImage: UIImageView!
    
    
}
