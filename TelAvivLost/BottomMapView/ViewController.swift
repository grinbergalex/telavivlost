//
//  ViewController.swift
//  HalfModalPresentationController
//
//  Created by Alex Grinberg on 31/01/19.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit
import MapKit
import Firebase

class ViewController: UIViewController, MKMapViewDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate  {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var sendLocationButton: UIButton!
    
    @IBOutlet weak var addressTextField: UITextField!
    
    let geocoder = CLGeocoder()
    
    var latitude: Double!
    var longitude: Double!
    
    var addressFinal = ""
    var itemNameback = ""
    var itemImageUrl = ""
    var itemDescriptionBack = ""
    
    let itemsToLoad = [Items]()
    
    var itemNameBack = ""
    var itemImageBack = UIImageView()
    var itemUidback = ""
    
    var manager: CLLocationManager!
    
    let locationManager = CLLocationManager()
    
    var regionRadius: CLLocationDistance = 1000
    
    let currentLocation: CLLocation! = nil
    
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: segue.destination)
        
        segue.destination.modalPresentationStyle = .custom
        segue.destination.transitioningDelegate = self.halfModalTransitioningDelegate
    }

}

