//
//  SecondViewController.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 22/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit

class AddOneItemVC: UIViewController {
    
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var itemTypeTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!

    @IBOutlet weak var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

