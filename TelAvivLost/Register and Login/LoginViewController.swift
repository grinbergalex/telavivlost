//
//  LoginViewController.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 25/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import UserNotifications

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    func handleLogin() {
        
        
        Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!, completion: {(user, error) in
            if error != nil {
               print("User could not login")
                //Missing notification at this part
                
            } else {
                print("User logged in")
            }
            
        })
        
    }
    
    @IBAction func loginUserAction(_ sender: UIButton) {
        
        SVProgressHUD.show()
        handleLogin()
        SVProgressHUD.dismiss()
        userLoggedInNotification()
        
        self.performSegue(withIdentifier: "AddItemsViewController", sender: self)
        
        
    }
}
