//
//  AppNavController.swift
//  HalfModalPresentationController
//
//  Created by Alex Grinberg on 31/01/19.
//  Copyright © 2018 Alex Grinberg. All rights reserved.
//

import UIKit

class AppNavController: UINavigationController, HalfModalPresentable {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return isHalfModalMaximized() ? .default : .lightContent
    }
}
