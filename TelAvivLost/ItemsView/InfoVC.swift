
//
//  InfoVC.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 24/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD
import MapKit

class InfoVC: UIViewController, UITableViewDelegate,UITableViewDataSource, MKMapViewDelegate, UINavigationControllerDelegate, UITextFieldDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descriptionView: UITextView!
    
    @IBOutlet weak var messageText: UITextField!
    
     var indexForAdd = AddItemsViewController?.self
    
    @IBOutlet weak var tableView: UITableView!
    
    var delegate: CenterVCDelegate?
    
    var messageToUser = ""
    var messageFromUser = ""
    
    
    let timestamp = NSDate().timeIntervalSince1970
    
    var resultArray: Array<Any> = []
    
    var messages = [Messages]()
    
    var uid = Auth.auth().currentUser?.uid
    
    var itemsToLoad = Items()
    var items = [Items]()
    var imageToLoad = [UserPersonalInformation]()
    
    var loadAddress = [AddressFromUser]()
    var address = AddressFromUser()
    
    var backInfoArray = [backInformation]()
    

    @IBOutlet weak var centerBtn: UIButton!

    var latitude: Double!
    var longitude: Double!
    
    var addressFinal = ""
    
    var itemNameBack = ""
    var itemDescriptionBack = ""
    var itemImageBack = UIImageView()
    var itemImageUrl = ""
    var itemUidback = ""
    
    var userId = Auth.auth().currentUser?.uid
    
    var locations = [AddressFromUser]()
    
    var loc = AddressFromUser()
    
    var backInfo = [backInformation]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        
        fetchPostsForItem()

    }
    
    let settingsLauncher = SettingsLauncher()
    @objc func handleMore() {
        settingsLauncher.showSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.nameLabel.text = itemsToLoad.itemName
        self.descriptionView.text = itemsToLoad.itemSubtitle
        self.userId = itemsToLoad.key
        self.itemImageUrl = itemsToLoad.itemImage
        
        //print(itemsToLoad.key)
        
        
        
        let url = URLRequest(url: URL(string: itemsToLoad.itemImage)!)
        
        let task = URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            
            if error != nil {
                print(error!)
                return
            }
            
            DispatchQueue.main.async {
                self.profileImageView.image = UIImage(data: data!)
            }
            
        }
        
        task.resume()

    //    fetchBackInfo()
        fetchPostsForItem()
        
     self.tableView.reloadData()
        

        
    }
    
    func fetchBackInfo() {
        
        let uid = Auth.auth().currentUser?.uid
        
        let db = Database.database().reference().child("selections").child(itemsToLoad.key!)
        db.observe(.value, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let info = backInformation()
                info.itemName = (dictionary["itemNameText"] as! String?)!
                info.itemInfomation = (dictionary["description"] as! String?)!
                info.itemImage = (dictionary["profileImageView"] as! String?)!
                self.backInfoArray.append(info)
                
                self.nameLabel.text = info.itemName
                self.descriptionView.text = info.itemInfomation
                
                let url = URLRequest(url: URL(string: info.itemImage)!)
                
                let data = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.profileImageView.image = UIImage(data: data!)
                        
                        }
                    self.returnUserAddress()
                    }
            
                    print(snapshot)
            
                }
        
            })
        }

    
    func returnUserAddress() {
        
        let address = AddressFromUser()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        vc.addressFinal = address.addressLabel
        
     //   vc.addressTextField.text = vc.addressFinal
    
        
    }
    
    @IBAction func locationPressed(_ sender: UIButton) {
        
        handleMore()
        
    }
    
    
   func fetchPostsForItem() {
        
    let DB_BASE = Database.database().reference().child("messages").child(uid!)
        DB_BASE.observe(DataEventType.childAdded, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = Messages()
                index.messageText = (dictionary["text"] as! String?)!
                //index.date = (dictionary["date"] as! String?)!
                self.messages.append(index)
                
                DispatchQueue.main.async(execute: {
                    self.tableView?.reloadData()
                })
            }
            print(snapshot)
        }, withCancel: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemChatCell", for: indexPath) as! ItemChatCell
        let info = messages[indexPath.row]
        cell.messagesText.text = info.messageText
        //cell.dateLabel.text = info.date
        
        return cell
        
    }
    
    @IBAction func sendMessage(_ sender: UIButton) {
        
        let values = ["itemId": itemsToLoad.key!, "text": messageText.text!, "fromUser": userId!, "timestamp": timestamp] as [String : Any]
        
        let messageRef = Database.database().reference()
        messageRef.child("messages").child(itemsToLoad.key!).childByAutoId().updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(user, error) in
            
            if error == error {
                print("message saved")
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            } else {
                print("could not save the message")
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
            
        })
        
        let userHistory = Database.database().reference()
        userHistory.child("userMessages").child(uid!).childByAutoId().updateChildValues(values as [AnyHashable : Any], withCompletionBlock: {(user, error) in
            
            if error == error {
                print("message saved")
            } else {
                print("could not save the message")
            }
            
        })
        

    }
    
    
    @IBAction func goBack(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "ItemsInfoVC", sender: self)
        
    }
    

    
}
