//
//  ModalViewController.swift
//  HalfModalPresentationController
//
//  Created by Alex Grinberg on 31/01/19.
//  Copyright © 2018 Alex Grinberg. All rights reserved.

import UIKit

class ModalViewController: UIViewController, HalfModalPresentable {
    @IBAction func maximizeButtonTapped(sender: AnyObject) {
        maximizeToFullScreen()
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        if let delegate = navigationController?.transitioningDelegate as? HalfModalTransitioningDelegate {
            delegate.interactiveDismiss = false
        }
        
        dismiss(animated: true, completion: nil)
    }
}
