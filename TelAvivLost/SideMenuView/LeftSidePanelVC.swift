//
//  SideVCViewController.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 26/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase

class LeftSidePanelVC: UIViewController {
    
    let uid = Auth.auth().currentUser?.uid
    
    var userProfileSideMenuArray = [UserProfilePictureFromSideMenu]()
    let userProfileSideMenu = UserProfilePictureFromSideMenu()

    @IBOutlet weak var logoSideView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }
    
    
    @IBAction func settingsClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SettingsVC", sender: self)
    }
    
    @IBAction func lostItemsClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "ItemInfoVC", sender: self)
    }
    
    @IBAction func foundItemsClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "FoundVC", sender: self)
    }
    
    @IBAction func addNewItemsClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "AddItemsViewController", sender: self)
    }
    
    @IBAction func messagesItemsClicked(_ sender: UIButton) {
        self.performSegue(withIdentifier: "UserMessages", sender: self)
    }
    
    
}
