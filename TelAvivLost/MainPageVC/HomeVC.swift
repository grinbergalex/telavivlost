//
//  MainVC.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 26/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import Firebase
import SVProgressHUD

class HomeVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var delegate: CenterVCDelegate?
    
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var nameTexField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var foundListButton: UIButton!
    
    @IBOutlet weak var lostListButton: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableView: UITableView!
    
    var information = [LoadMainInfo]()
    
    var loadAddedItem = [LoadAddedItems]()
    
    var messages = [Messages]()
    
    let uid = Auth.auth().currentUser?.uid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show()
        
        fetchUserInfo()
        fetchUserItemsAdded()
        fetchUserMessages()
        
        SVProgressHUD.dismiss()

        // Do any additional setup after loading the view.
    }

    func fetchUserInfo() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let childId = Database.database().reference().child("users").child(uid)
        childId.observeSingleEvent(of: .value, with: { (snapshot) in

            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = LoadMainInfo()
                index.username = ((dictionary["name"]) as? String)!
                index.useremail = ((dictionary["email"]) as? String)!
                index.url = ((dictionary["profileImageUrl"]) as? String)!
                self.information.append(index)
                self.nameTexField.text = index.useremail
                self.emailTextField.text = index.useremail
                self.passwordTextField.text = index.useremail
                
                let url = URLRequest(url: URL(string: index.url)!)
                
                let task = URLSession.shared.dataTask(with: url) {
                    (data, response, error) in
                    
                    if error != nil {
                        print(error!)
                        return
                    }
                    
                    DispatchQueue.main.async {
                        self.profileImageView.image = UIImage(data: data!)
                    }
                    
                }
                
                task.resume()
                
                
            }
            print(snapshot)
            
        }, withCancel: nil)
        
    }
    
    func fetchUserItemsAdded() {
        
        guard let uid = Auth.auth().currentUser?.uid else {return}
        let childId = Database.database().reference().child("userItemsAdded").child(uid)
        childId.observe(.childAdded, with: {(snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = LoadAddedItems()
                index.itemName = ((dictionary["itemName"]) as? String)!
                index.itemSubtitle = ((dictionary["email"]) as? String)!
                index.itemImage.image = (dictionary["profileImageUrl"]) as? UIImage
            }
        
            print(snapshot)
            
        }, withCancel: nil)
    }
    
    func fetchUserMessages() {
        
        let childId = Database.database().reference().child("userMessages").child(uid!)
        childId.observe(.childAdded, with: {(snapshot) in
            
            if let dictionary = snapshot.value as? [String: AnyObject] {
                let index = Messages()
                index.messageText = ((dictionary["text"]) as? String)!
             //   index.date = ((dictionary["timestamp"]) as? String)!
                self.messages.append(index)
                
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                    })
            }
            
            print(snapshot)
            
            
            
        }, withCancel: nil)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesCell", for: indexPath) as! MessagesCell
        let index = messages[indexPath.row]
        cell.messageTextField.text = index.messageText
        cell.date.text = index.date
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return loadAddedItem.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserItemsCollectionCell", for: indexPath) as! UserItemsCollectionCell
        let index = loadAddedItem[indexPath.row]
        cell.itemName.text = index.itemName
        cell.itemImageView.image = index.itemImage.image
        
        return cell
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        delegate?.toggleLeftPanelVC()
    }
    
}
