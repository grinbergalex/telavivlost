//
//  BottomViewController.swift
//  TelAvivLost
//
//  Created by Alex Grinberg on 29/01/2019.
//  Copyright © 2019 Alex Grinberg. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate {
    
    var itemNameback = ""
    var itemImageUrl = ""
    var itemDescriptionBack = ""
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var sendLocationButton: UIButton!
    
    @IBOutlet weak var addressTextField: UITextField!
    
    let geocoder = CLGeocoder()

    var latitude: Double!
    var longitude: Double!
    
    var addressFinal = ""
    
    let itemsToLoad = [Items]()

    var itemNameBack = ""
    var itemImageBack = UIImageView()
    var itemUidback = ""
    
    var manager: CLLocationManager!
    
    let locationManager = CLLocationManager()
    
    var regionRadius: CLLocationDistance = 1000
    
    let currentLocation: CLLocation! = nil
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

    self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
        
        mapView.delegate = self
        mapView.mapType = .standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        getUserLocation()
        
        addressTextField.delegate = self
        
        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
    }
    

    
    func getImage(url: String, completion: @escaping (UIImage?) -> ()) {
        URLSession.shared.dataTask(with: URL(string: url)!) { data, response, error in
            if error == nil {
                completion(UIImage(data: data!))
            } else {
                completion(nil)
            }
            }.resume()
    }
    
    func returnUserAddress() {
        
        let address = AddressFromUser()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        vc.addressFinal = address.addressLabel
        
    }

    @IBAction func centerBtnWasPressed() {
        getUserLocation()
        
        getAddress { (address) in
            print(address)
            self.addressFinal = address
            print(self.addressFinal)
            self.addressTextField.text = address
            
            self.animate()
        }
    }
        
    func centerMapOnUserLocation() {
        
        getUserLocation()
        print(mapView.userLocation.coordinate)
    }
    
    func getUserLocation() {
        
        let locationManager = CLLocationManager()
        
        locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            print(mapView.userLocation.coordinate)
            //Need to add an update function
            
            
            
        }
        
    }
    
    func getAddress(handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: (locationManager.location?.coordinate.latitude)!, longitude: (locationManager.location?.coordinate.longitude)!)
        //selectedLat and selectedLon are double values set by the app in a previous process
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // Address dictionary
            //print(placeMark.addressDictionary ?? "")
            
            // Location name
            if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                address += locationName + ", "
            }
            
            // Street address
            if let street = placeMark?.addressDictionary?["Thoroughfare"] as? String {
                address += street + ", "
            }
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address += city + ", "
            }
            
            // Zip code
            if let zip = placeMark?.addressDictionary?["ZIP"] as? String {
                address += zip + ", "
            }
            
            // Country
            if let country = placeMark?.addressDictionary?["Country"] as? String {
                address += country
            }
            
            // Passing address back
            handler(address)
            
            
        })
    }
    //MARK: Need to check this function. Not sure it's working
    @IBAction func sendLocation(_ sender: UIButton) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "InfoVC") as! InfoVC
        vc.nameLabel.text = itemNameBack
        vc.descriptionView.text = itemDescriptionBack
        vc.itemImageUrl = itemImageUrl
        
        let photoUrl =  itemImageUrl  //posts[indexPath.row].photoUrl
        
        getImage(url: photoUrl) { photo in
            
                    DispatchQueue.main.async {
                        self.itemImageBack.image = photo
                    }
                }
            }
    
    func animate() {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.addressTextField.transform = CGAffineTransform(translationX: -30, y: 0)
        }) { (_) in
            
        }
    }
    
    
    
    
    
    func directions() {
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude), addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: 37.783333, longitude: -122.416667), addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile
        
        let directions = MKDirections(request: request)
        print(request.source!)
        
        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }
            
            for route in unwrappedResponse.routes {
                self.mapView.addOverlay(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        return renderer
    }
    
}
extension MapViewController {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
        if status == .authorizedWhenInUse {
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        mapView.mapType = MKMapType.standard
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = locValue
        annotation.title = "Javed Multani"
        annotation.subtitle = "current location"
        mapView.addAnnotation(annotation)
        
        
        //centerMap(locValue)
    }
    
}
